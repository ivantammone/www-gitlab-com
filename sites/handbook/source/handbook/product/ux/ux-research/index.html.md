---
layout: handbook-page-toc
title: "UX research at GitLab"
description: "The goal of UX research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations, and goals."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The goal of UX research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations, and goals. We use these insights to inform and strengthen product and design decisions.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/EQ750KX_6nU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

UX Researchers are one of the many GitLab Team Members who conduct user research. Other roles, such as Product Managers and Product Designers, frequently conduct research with guidance from the UX Research team.

Do you have questions about UX Research? The UX Research team is here for you! Reach out in the #ux_research Slack channel.

Below is a full list of our UX Research team handbook content:

### Conducting UX Research at GitLab

- [Defining goals, objectives, and hypotheses](/handbook/product/ux/ux-research-training/defining-goals-objectives-and-hypotheses/)
- [Problem Validation and methods](/handbook/product/ux/ux-research-training/problem-validation-and-methods/)
- [Solution Validation and methods](/handbook/product/ux/ux-research-training/solution-validation-and-methods/)
- [Foundational research](/handbook/product/ux/ux-research-training/foundational-research/)
- [Strategic research at GitLab](/handbook/product/ux/ux-research-training/strategic-research-at-gitlab/)
- [UX Research tools/applications](/handbook/product/ux/ux-research-training/research-tools/)
- [UX Cloud Sandbox](/handbook/product/ux/ux-research-training/ux-cloud-sandbox/)

### Research methods we use at GitLab

- [Choosing a methodology](/handbook/product/ux/ux-research-training/choosing-a-research-methodology/)
- [Longitudinal studies](/handbook/product/ux/ux-research-training/longitudinal-studies/)
- [Diary studies](/handbook/product/ux/ux-research-training/diary-studies/)
- [Kano Model for feature prioritization](/handbook/product/ux/ux-research-training/kano-model/)
- [User story mapping](/handbook/product/ux/ux-research-training/user-story-mapping/)
- [Rapid Iterative Testing and Evaluation (RITE)](/handbook/product/ux/ux-research-training/rite/)
- [Usability testing](/handbook/product/ux/ux-research-training/usability-testing/)
- [Usability benchmarking](/handbook/product/ux/ux-research-training/usability-benchmarking/)
- [Unmoderated usability testing](/handbook/product/ux/ux-research-training/unmoderated-testing/)
     - [Writing a website usability testing script](/handbook/product/ux/ux-research-training/writing-usability-testing-script/)
- [Preference testing](/handbook/product/ux/ux-research-training/preference-testing/)
- [Facilitating user interviews](/handbook/product/ux/ux-research-training/facilitating-user-interviews/)
     - [Writing a discussion guide for user interviews](/handbook/product/ux/ux-research-training/discussion-guide-user-interviews/)
- [How to create a user persona](/handbook/product/ux/persona-creation/)
- [Evaluating navigation](/handbook/product/ux/ux-research-training/evaluating-navigation/)
     - [Testing navigation: early Solution Validation](/handbook/product/ux/ux-research-training/early-solution-validation-process-for-navigation/)
         - [First click testing for navigation](/handbook/product/ux/ux-research-training/first-click-testing/)
              - [Creating a first click study in Qualtrics](/handbook/product/ux/ux-research-training/creating-first-click-study-qualtrics/)
         - [RITE testing for navigation](/handbook/product/ux/ux-research-training/using-rite-to-test-navigation/)
         - [Comparative testing for navigation](/handbook/product/ux/ux-research-training/comparative-testing-for-navigation/)

### Finding participants

- [How to write an effective screener](/handbook/product/ux/ux-research-training/write-effective-screener/)
     - [The Common Screener: an efficient way to screen for multiple studies](/handbook/product/ux/ux-research-training/recruiting-participants/common-screener/)
- [Recruiting participants](/handbook/product/ux/ux-research-training/recruiting-participants/)
- [Using the UX Research Google Calendar](/handbook/product/ux/ux-research-training/ux-research-google-calendar/)
- [Attending a research event](/handbook/product/ux/ux-research-training/attending-a-research-event/)
- [Creating and managing a research participant panel](/handbook/product/ux/ux-research-training/research-panel-management/)

### Data and research insights

- [Finding existing research](/handbook/product/ux/ux-research-training/finding-existing-research/)
- [Collecting useful data](/handbook/product/ux/ux-research-training/collecting-useful-data/)
- [Using quantitative data to find insights](/handbook/product/ux/ux-research-training/quantitative-data/)
- [Analyzing and synthesizing user research data](/handbook/product/ux/ux-research-training/analyzing-research-data/)
- [Research insights](/handbook/product/ux/ux-research-training/research-insights/)
- [Documenting research insights in Dovetail](/handbook/product/ux/dovetail/)

### Templates

- [UX research report template (internal link)](https://docs.google.com/presentation/d/1E8eZpf0T3p6Wf-aEmLjIOFZ_6jdvxc4eySwQ6FnHCZs/copy)
- [User interview note-taking template (internal link)](https://docs.google.com/spreadsheets/d/1_zFp_WXg9jM84dBqv4ARPFTtwPlJGxAi_IVDeED8VFY/copy)
- [Usability testing script template (internal link)](https://docs.google.com/document/d/15tvKXmFUxOT7fo550efuFLQ_ZSDZ2fyuVX_XTQSDBJk/copy)
- [Usability testing rainbow analysis chart template (internal link)](https://docs.google.com/spreadsheets/d/1_bGO9uUxWL5dKe5r1vxTo4J4QAEHfp6mu7VIQDsTu_E/copy)
- [Recommendations alignment Mural template (internal link)](https://app.mural.co/template/a5b1cf65-483d-4014-8681-373e1a6c9ee7/c2c2b37a-baf6-4b5f-9e97-87f132d07e19)
- [Usability benchmarking alignment Mural template (internal link)](https://app.mural.co/template/6c725b01-a3be-422e-8cec-d8823e9aaa73/a9f6620d-47cb-43d1-b2a9-c980c2d51308)

### Checklists

- [User interviews (internal link)](https://docs.google.com/document/d/1Sg0-4U5W_iop-W1TWDZiECkGkhRkscLgsC-jFUhytBM/copy)
- [Surveys (internal link)](https://docs.google.com/document/d/1Rj8LZuj-ATDKFt19F0dKy6J6-FokChMgCyr7OvfqZ_k/copy)
- [Usability testing (internal link)](https://docs.google.com/document/d/14UWLmbZwVwHkTf1Ncza90WFWk4zLN05fglnNAP4oL9w/copy)

### UX Research training

- [Interview Carousel - Becoming a better interviewer 15 minutes at a time](/handbook/product/ux/ux-research-training/interview-carousel/)
- [Qualtrics tips and tricks](/handbook/product/ux/qualtrics/)
- [UX Research shadowing](/handbook/product/ux/ux-research-training/research-shadowing/)

### Resources for UX Researchers

- [How the UX Research team operates at GitLab](/handbook/product/ux/ux-research-training/how-uxr-team-operates/)
- [Research prioritization](/handbook/product/ux/ux-research-training/research-prioritization/)
- [The IP Assignment and when to show it](/handbook/product/ux/ux-research-coordination/IP-Assignment/)
- [How to fill in for a UX Research Operations Coordinator](/handbook/product/ux/ux-research-coordination/research-coordinator-fill-in/)
- [UX Research growth and development](/handbook/product/ux/ux-research-training/ux-research-growth-and-development/)
- [Non-Disclosure Agreements for UX Research](/handbook/product/ux/ux-research-coordination/NDAs/)
- [How to publish a blog post to encourage community contributions on Actionable Insights](/handbook/product/ux/ux-research-training/community-contributions-for-actionable-insights/)

### Resources for UX Research Operations Coordinators

- [UXR Operations Coordination at GitLab](/handbook/product/ux/ux-research-coordination/)
- [First Look UX Research panel](/handbook/product/ux/ux-research-coordination/first-look-ux-research-panel/)
- [Finding SaaS users](/handbook/product/ux/ux-research-training/finding-saas-users/)
- [UX research recruiting email tips](/handbook/product/ux/ux-research-training/recruiting-participants/recruiting-email-tips/)

### Measures and processes the UX Research team is responsible for

- [System Usability Scale](/handbook/product/ux/performance-indicators/system-usability-scale/)
     - [System Usability Scale responder outreach](/handbook/product/ux/performance-indicators/system-usability-scale/sus-outreach.html)
     - [System Usability Scale database](/handbook/product/ux/sus-database/)
- [Category Maturity Scorecards](/handbook/product/ux/category-maturity-scorecards/)
- [Tracking research velocity](/handbook/product/ux/ux-research-training/tracking-research-velocity/) 
- [Tracking gold, silver, and bronze UX research projects](/handbook/product/ux/ux-research-training/tracking-research-projects/) 

